class Player(object):
    def __init__(self):
        self.tracks = []
        self.counter = 0

    def addTrack(self, trackInfo):
        self.tracks.append(trackInfo)

    def play(self):
        print('Now playing: {} by {}'.format(self.tracks[self.counter].title, self.tracks[self.counter].artist))

    def nextTrack(self):
        if self.counter == len(self.tracks) - 1:
            self.counter = 0
        else:
            self.counter += 1

    def previous(self):
        if self.counter == 0:
            self.counter = len(self.tracks) - 1
        else:
            self.counter -= 1

    def printAllTracks(self):
        for track in self.tracks:
            print('Track {}: "{}" by "{}"'.format(self.tracks.index(track), track.title, track.artist))

    def selectTrack(self, number):
        print('Now playing: "{}" by "{}"'.format(self.tracks[number].title, self.tracks[number].artist))
        self.counter = number


class Track(object):
    def __init__(self, artist, title, album):
        self.artist = artist
        self.title = title
        self.album = album

    def printInfo(self):
        print('{}\nTitle: {}\nAlbum: {}'.format(self.artist, self.title, self.album))


player = Player()

nirvanaTrack = Track('Nirvana', 'In Bloom', 'Nevermind')
radioheadTrack = Track('Radiohead', 'Fake Plastic Trees', 'The Bends')
nationalTrack = Track('The National', 'Bloodbuzz Ohio', 'High Violet')
vanEttenTrack = Track('Sharon van Etten', 'Give Out', 'Tramp')
arcadeFireTrack = Track('Arcade Fire', 'Suburban War', 'The Suburbs')

player.addTrack(nirvanaTrack)
player.addTrack(radioheadTrack)
player.addTrack(nationalTrack)
player.addTrack(vanEttenTrack)
player.addTrack(arcadeFireTrack)


if __name__ == "__main__":
    player.play()
    player.nextTrack()
    player.play()
    player.nextTrack()
    player.play()
    player.previous()
    player.play()
    player.previous()
    player.previous()
    player.play()

    player.printAllTracks()

    player.selectTrack(3)

